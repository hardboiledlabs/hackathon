
circles_collection = new Meteor.Collection('circle');

current_button_collection = new Meteor.Collection('current_button');

current_tilt_collection = new Meteor.Collection('current_tilt');


if(Meteor.isClient) {



  Router.map(function() {
    this.route('home', {path: '/'})
    this.route('how_to', {path: '/how_to'});
  });

  // Meteor.startup( function() {
  //   Reveal.initialize();
  // });

  

  var circles_list;
  Template.click_area.events({
    'click ':function(e){
      //console.log(e.pageX);
      //init_circles(500);
    },
    'mousedown':function(e){
        mouse_down_function();
    },
    'mouseup':function(e){
        mouse_up_function();
    },
    'click .save_circles':function(e){
      var circles_elements = $('circle');
      var title = $('.title')[0].value;
      var i=0;
      var circles_array = [];
      for(i=0;i<circles_elements.length;i++){
        var cirlce_element = circles_elements[i];
        var circle_object = {
          radius: cirlce_element.r.baseVal.value,
          color: cirlce_element.style.fill,
          cx: cirlce_element.cx.animVal.value,
          cy: cirlce_element.cy.animVal.value,
          date: new Date()
        };
        circles_array.push(circle_object);
      }
      var circles_object = {
          title: title,
          //user: Meteor.userId(),
          circles_array: circles_array,
          date: new Date()
      }
      Meteor.call('insert_circles', circles_object);
    }

  });


  function mouse_down_function(){
        if(typeof first_mouse_down === 'undefined'){
          init_circles(500);
          first_mouse_down = false;
       }
       else{
          if(first_mouse_down){
            init_circles(500); 
            first_mouse_down = false;
          }
       }
       var radius = 10;
       change_radius_interval = setInterval(function(){
          change_radius(radius++);
       },20);

       // play_midi = setInterval(function(){
       //    ///play_sound();
       // }, 10);
       //play_midi = play_sound();
  }

  function mouse_up_function(){
      if(typeof first_mouse_down !== 'undefined'){
        clearInterval(change_radius_interval);
        circle_svg.attr('fill-opacity', 0.68);
        // if(circles_list) circles_list.push(circle_svg);
        // else circles_list = [circle_svg];

        //if reaches the boundary, shift all circles to the left whenever a new circle is created
        if($('circle')[$('circle').length-1].getBoundingClientRect().right > window.innerWidth * 0.8){
          var shift_length = window.innerWidth * 0.2;
          if(typeof shift_left === "undefined") shift_left = -shift_length;
          else shift_left -= shift_length;
          svg_g
            .attr("transform", "translate(" + shift_left + ",0)");
        }
        first_mouse_down = true;
        //clearInterval(play_midi); 
        if(typeof play_sound_file !== 'undefined'){
          play_sound_file.pause();
          play_sound_file.currentTime = 0;
        }
      }
  }

  function change_radius(radius){
    if(typeof circle_svg !== 'undefined'){
      circle_svg
        .attr('fill-opacity', 1)
        .attr('r', radius);
    }
    else{
      //init_circles(500);
    }

  }

  // function play_sound(){
  //   var note_interval = 5;
  //   var note =48; //green
  //   if(random_number<0.25) {note = 50; $('#audio1')[0].play();} //red 
  //   else if(random_number<0.5) {note = 52; $('#audio2')[0].play();} //blue
  //   else if(random_number<0.75) {note = 55; $('#audio3')[0].play();} //orange
  //   else {$('#audio4')[0].play();}
  //   //console.log(MIDI);
  //   //MIDI.noteOn(0, note, 60);
  // }


  function init_circles(new_data, tiltState){
    // var data = Session.get('data_array');
    // if(!data){
    //   data = [];
    // }
    // data.push(new_data);
    // Session.set('data_array', data);
    // if(!Session.get('circle_index')){
    //   Session.set('circle_index', 5);
    // }
    // else{
    //   Session.set('circle_index', Session.get('circle_index') + 1);
    // }
    // circle_index = Session.get('circle_index');
    if(typeof circle_index === 'undefined') circle_index = 5;
    else circle_index++;

    data = [new_data];

    //var svg = d3.select("svg");
    svg_g = d3.select("g");
    circle_svg = svg_g.selectAll("circles")
    .data(data);

    var random_color; 
    //random_number = Math.random();
    var tiltState = Session.get('tiltState');
    console.log('tiltState '+ tiltState);
    console.log('Session.get() ' + Session.get('tiltState'));
    if(tiltState === '1' || tiltState === 1) {
      random_color = "f35a40";  
      play_sound_file = $('#audio1')[0];

     } //red 
    else if(tiltState === '2' | tiltState === 2){
      random_color = "fbaf37"; 
      play_sound_file = $('#audio2')[0];
    } //orange
    else if(tiltState == '3' | tiltState === 3){
      random_color = "03b3ed"; 
      play_sound_file = $('#audio3')[0];
    } //blue
    else if(tiltState == '4' | tiltState === 4) {
      random_color = "7ed432"; 
      play_sound_file = $('#audio4')[0];
    } //green
    else{
      console.log('tiltState ' + tiltState);
    }
    //play_sound_file && play_sound_file.muted = false;
    if(typeof play_sound_file !== 'undefined') play_sound_file.play();

    circle_svg.enter()
    .append("circle")
    .attr({
      cx: function(d,i) { return circle_index*50 }, 
      cy: 300, 
      r: function(d,i) { return d/30 },
      'fill-opacity': 0.2
    })
    .style("fill", random_color);
  }

  Template.saved_circles.circles = function(){
    var circles = circles_collection.find().fetch();
    return circles;
  }

  Template.circles.rendered = function(){
    // MIDI;
    // MIDI.loadPlugin({
    //   instrument: "synth_drum", // or 1 (default)
    //   //instruments: [ "acoustic_grand_piano", "acoustic_guitar_nylon" ], // or multiple instruments
    //   callback: function() { }
    // });

  }


  Deps.autorun(function(){
      //current_button_collection.find();
      if(current_button_collection.find().fetch()[0]){
        var query = current_button_collection.find();
        var handle = query.observeChanges({
          changed: function(id,result) {
              //console.log(result.buttonState + ' ' + result.tiltState);
              var buttonState = result.buttonState;
              if(buttonState){
                mouse_down_function();
                //console.log('mouse_down_function();');
              }
              else{
                mouse_up_function();
                //console.log('mouse_up_function();');
              }
          }
        });
      }

      if(current_tilt_collection.find().fetch()[0]){
        var query = current_tilt_collection.find();
        var handle = query.observeChanges({
          changed: function(id,result) {
              tiltState = current_tilt_collection.find().fetch()[0].tileState;
              Session.set('tiltState', tiltState);
              console.log(current_tilt_collection.find().fetch()[0].tileState);
              console.log('tiltState '+tiltState);
              if(typeof tiltState !== 'undefined' && tiltState !== 'undefined'){
                tiltState = result.tiltState;
              }
          }
        });
      }
    });



}

if (Meteor.isServer) {
  Meteor.startup(function () {
    var com = Meteor.require("serialport")
    var serialPort = null;

    //Tracks the state of tilt
    var tiltState = 0;

    //Tracks button
    var buttonState = false;

    // Depending on what platform we're using lets open up the Arduino
    if ( process.platform == "darwin" ) {
        var serialPort = new com.SerialPort("/dev/tty.usbmodem1411", {
          baudrate: 38400
        });
    }
    // If the serial port is valid lets open it up!
    if (serialPort) {
        // Callback for when the serial port gets opened
        serialPort.on('open', function() {
           console.log(new Date() + " serial - serial open");

           // Callback for when serial data is recieved
           serialPort.on('data', function(data) {

              //console.log(data[0].toString(16));

              //Setting the button state
              if( data[0] & 0x10 ) {
                buttonState = true;
              } else {
                buttonState = false;
              }

              //Setting the tilt state
              if( data[0] & 0x01 ) {
                tiltState = 1;  //Red
              } else if (data[0] & 0x02) {
                tiltState = 2;  //Yellow
              } else if (data[0] & 0x04) {
                tiltState = 3;  //Blue
              } else if (data[0] & 0x08) {
                tiltState = 4;  //Green
              } else {
                tiltState = 0;  //Off
              }

              //console.log('buttonState ' + buttonState);
              //console.log('tiltState ' + tiltState);

              if((typeof buttonState !== 'undefined') && (buttonState !== 'undefined')){
                var Fiber = Npm.require('fibers');
                Fiber(function(){
                   Meteor.call('update_current_button', buttonState);
                   //console.log('function called');
                }).run();
              }

              if((typeof tiltState !== 'undefined') && (tiltState !== 'undefined')){
                  var Fiber = Npm.require('fibers');
                  Fiber(function(){
                     Meteor.call('update_current_tilt', tiltState);
                     //console.log('function called');
                  }).run();

              }

           });

        });
    }


    // some_num = 1;
    // setInterval(function(){
    //   console.log(some_num);
    //   var Fiber = Npm.require('fibers');
    //   Fiber(function(){
    //     Meteor.call('update_current_signal', some_num++, some_num);
    //   }).run();
    // }, 300);

  });

}


Meteor.methods({
  'insert_circles':function(circles_object){
    circles_collection.insert(circles_object);
  },
  'update_current_button':function(buttonState){
    current_button_collection.upsert({},{
      buttonState: buttonState
    });
  },
   'update_current_tilt':function(tileState){
    current_tilt_collection.upsert({},{
      tileState: tileState
    });
  } 
})







