/* Copyright (c) 2009 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 * @defgroup uart_example_pca10000_main main.c
 * @{
 * @ingroup uart_example_pca10000
 *
 * @brief UART Example Application main file.
 *
 * This file contains the source code for a sample application using UART.
 *
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include "nrf.h"
#include "simple_uart.h"
#include "nrf_gpio.h"
#include "nrf_esb.h"
#include "nrf_delay.h"
#include "boards.h"

/*****************************************************************************/
/** @name Configuration */
/*****************************************************************************/

// Define pipe
#define PIPE_NUMBER 0 ///< We use pipe 0 in this example

// Define payload length
#define TX_PAYLOAD_LENGTH 2 ///< We use 1 byte payload length when transmitting

// Data and acknowledgement payloads
static uint8_t my_rx_payload[NRF_ESB_CONST_MAX_PAYLOAD_LENGTH]; ///< payloads from PRX.
volatile bool received_data = 0;


void simple_uart_print_int(int i);

/**
 * @brief Function for application main entry.
 * @return 0. int return type required by ANSI/ISO standard.
 */
int main(void)
{
    simple_uart_config(RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, HWFC);
    simple_uart_putstring((const uint8_t*)"Starting...\r\n");

      // Initialize ESB
    (void)nrf_esb_init(NRF_ESB_MODE_PRX);

    // Enable ESB to start receiving
    (void)nrf_esb_enable();

    while (1) {
        // Optionally set the CPU to sleep while waiting for a callback.
        // __WFI();

        while (!received_data); // wait

        //Print out the goodies.
        simple_uart_print_int(my_rx_payload[0]);
        simple_uart_putstring((const uint8_t*)"\r\n");

        received_data = 0;
        nrf_esb_flush_rx_fifo(PIPE_NUMBER);
    }
}

void nrf_esb_tx_success(uint32_t tx_pipe, int32_t rssi){}

void nrf_esb_rx_data_ready(uint32_t rx_pipe, int32_t rssi)
{
    uint32_t my_rx_payload_length;

    // Fetch packet and write first byte of the payload to the GPIO port.
    (void)nrf_esb_fetch_packet_from_rx_fifo(PIPE_NUMBER, my_rx_payload, &my_rx_payload_length);

    received_data = 1;
}

// Callbacks not needed in this example.
void nrf_esb_tx_failed(uint32_t tx_pipe) {}
void nrf_esb_disabled(void) {}


void simple_uart_print_int(int i)
{
    char buf[12]; // 2**32 = 4G => sign + 10 numbers + '\0'
    sprintf(buf, "%d" , i);
    simple_uart_putstring((const uint8_t *)buf);
}

