Eggbeats by Hardboiled Hardware
===============================

A motion and pressure-sensitive egg that creates music, connected to a mobile interface that records sounds as colors.
For kids, and kids at heart!

More details here: [hackster.io/hardboiled-hardware/egg-beats](http://www.hackster.io/hardboiled-hardware/egg-beats)

Tech details here: [Hardware](https://www.hackster.io/cedric/twi)
- [Firmware](https://github.com/honnet/twi).


Licence
-------

Attribution-NonCommercial-ShareAlike 2.0 Generic (CC BY-NC-SA 2.0):

[creativecommons.org/licenses/by-nc-sa/2.0](https://creativecommons.org/licenses/by-nc-sa/2.0)
